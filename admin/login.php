
<?php 
session_start();

if (isset($_SESSION['admin'])) {
   header('location: admin.php');
}




 ?>


<!DOCTYPE html>
<html>
<head>
	<title>Login</title>

	 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Avicola Garcia</title>

    <!-- Bootstrap Core CSS -->
    <link href="../asset/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="../asset/css/clean-blog.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../asset/font/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

   <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    Menu <i class="fa fa-bars"></i>
                </button>
                <a style="color: black;" class="navbar-brand" href="index.html">Avicola Garcia</a>
                 
                      <!--  <a style="color: black;" class="navbar-brand" href="validar/validar.php"></a>-->
                    
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a style="color: black;" href="../index.php">Inicio</a>
                    </li>
                    <li>
                        <a style="color: black;" href="../about.php">Sobre mi</a>
                    </li>
                   
                    <li>
                        <a style="color: black;" href="../contact.php">Contacto</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <br><br><br><br>


    	<h2 class="post-tittle" style="text-align: center;">Login</h2><br>

    	<?php 

    	if (isset($_GET['error'])) {
    		echo '<center><p>Datos incorrectos</p></center>';
    	}

    	 ?>

    	<center>
    		<form action="validar.php" method="post" >

    		<div class="form-group">
    			<input type="text" name="user" placeholder="Usuario" required="required">

    			</div>
    			
    			<div class="form-group">
    			<input type="password" name="pass" placeholder="Contraseña" required="required">
    			</div>

    			
    			 <button type="submit" class="btn btn-primary">Entrar</button>   			

    		

    		</form>

    		</center>


      <!-- jQuery -->
    <script src="../asset/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../asset/bootstrap/js/bootstrap.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="../asset/js/jqBootstrapValidation.js"></script>
    <script src="../asset/js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="../asset/js/clean-blog.min.js"></script>

</body>
</html>