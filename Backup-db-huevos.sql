-- MySQL dump 10.16  Distrib 10.1.16-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: huevos
-- ------------------------------------------------------
-- Server version	10.1.16-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cedula_nit` int(11) NOT NULL,
  `nombres` varchar(50) NOT NULL,
  `vendedor` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (5,0,'liris licona',2);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturas`
--

DROP TABLE IF EXISTS `facturas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `planilla` int(11) NOT NULL,
  `numero_factura` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `cliente` varchar(100) NOT NULL,
  `nit` int(11) NOT NULL,
  `tel` int(11) NOT NULL,
  `direccion` text NOT NULL,
  `bi` int(11) NOT NULL,
  `by_valor` int(11) NOT NULL,
  `baa` int(11) NOT NULL,
  `baa_valor` int(11) NOT NULL,
  `ba` int(11) NOT NULL,
  `ba_valor` int(11) NOT NULL,
  `bb` int(11) NOT NULL,
  `bb_valor` int(11) NOT NULL,
  `cb` int(11) NOT NULL,
  `cb_valor` int(11) NOT NULL,
  `jumbo` int(11) NOT NULL,
  `jumbo_valor` int(11) NOT NULL,
  `ry` int(11) NOT NULL,
  `ry_valor` int(11) NOT NULL,
  `raa` int(11) NOT NULL,
  `raa_valor` int(11) NOT NULL,
  `ra` int(11) NOT NULL,
  `ra_valor` int(11) NOT NULL,
  `rb` int(11) NOT NULL,
  `rb_valor` int(11) NOT NULL,
  `rc` int(11) NOT NULL,
  `rc_valor` int(11) NOT NULL,
  `rd` int(11) NOT NULL,
  `rd_valor` int(11) NOT NULL,
  `picados` int(11) NOT NULL,
  `picados_valor` int(11) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `total` int(11) NOT NULL,
  `vendedor` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturas`
--

LOCK TABLES `facturas` WRITE;
/*!40000 ALTER TABLE `facturas` DISABLE KEYS */;
INSERT INTO `facturas` VALUES (37,1,1856,'2017-03-25','liris licona',0,0,'0',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,300,250,0,0,0,0,0,0,0,0,'credito',75000,2),(38,1,1857,'2017-03-25','yaneth carvajal',0,0,'0',0,0,0,0,0,0,0,0,0,0,0,0,0,0,180,285,0,0,0,0,0,0,0,0,0,0,'credito',51300,2),(39,1,1258,'2017-03-25','wilson pineda',0,0,'0',0,0,0,0,0,0,0,0,0,0,0,0,210,310,600,260,0,0,0,0,0,0,0,0,0,0,'credito',221100,2),(40,1,1859,'2017-03-25','guillermo martin',0,0,'0',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,300,240,0,0,0,0,0,0,0,0,'credito',72000,2),(41,1,1860,'2017-03-25','yaneth chacon',0,0,'0',0,0,0,0,0,0,0,0,0,0,0,0,150,310,0,0,0,0,0,0,0,0,0,0,0,0,'credito',46500,2),(42,1,1861,'2017-03-25','carlos bentacur',0,0,'0',0,0,0,0,0,0,0,0,0,0,0,0,120,310,0,0,150,240,0,0,0,0,0,0,0,0,'credito',73200,2),(43,1,1862,'2017-03-25','diego henao',0,0,'0',0,0,0,0,0,0,0,0,0,0,0,0,0,0,1500,255,0,0,0,0,0,0,0,0,0,0,'credito',382500,2),(44,1,1863,'2017-03-25','barvara gonzales',0,0,'0',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,450,240,0,0,0,0,0,0,0,0,'credito',108000,2),(45,1,1864,'2017-03-25','contado',0,0,'0',0,0,0,0,0,0,0,0,0,0,0,0,0,0,1350,260,360,240,240,240,0,0,0,0,0,0,'cancelado',495000,2);
/*!40000 ALTER TABLE `facturas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ini`
--

DROP TABLE IF EXISTS `ini`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ini` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yumbo_blanco` int(11) NOT NULL,
  `yumbo_rojo` int(11) NOT NULL,
  `extra_blanco` int(11) NOT NULL,
  `extra_rojo` int(11) NOT NULL,
  `aa_blanco` int(11) NOT NULL,
  `aa_rojo` int(11) NOT NULL,
  `a_blanco` int(11) NOT NULL,
  `a_rojo` int(11) NOT NULL,
  `b_blanco` int(11) NOT NULL,
  `b_rojo` int(11) NOT NULL,
  `c_blanco` int(11) NOT NULL,
  `c_rojo` int(11) NOT NULL,
  `pipo_blanco` int(11) NOT NULL,
  `pipo_rojo` int(11) NOT NULL,
  `otros_blanco` int(11) NOT NULL,
  `otros_rojo` int(11) NOT NULL,
  `vendedor` int(11) NOT NULL,
  `planilla` int(11) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ini`
--

LOCK TABLES `ini` WRITE;
/*!40000 ALTER TABLE `ini` DISABLE KEYS */;
INSERT INTO `ini` VALUES (22,0,0,0,1620,0,1890,0,600,0,300,0,0,0,0,0,0,2,1,'2017-03-25');
/*!40000 ALTER TABLE `ini` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `planilla_diaria`
--

DROP TABLE IF EXISTS `planilla_diaria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planilla_diaria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yumbo_blanco` int(11) NOT NULL,
  `yumbo_rojo` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `vendedor` int(100) NOT NULL,
  `extra_blanco` int(11) NOT NULL,
  `extra_rojo` int(11) NOT NULL,
  `aa_blanco` int(11) NOT NULL,
  `aa_rojo` int(11) NOT NULL,
  `a_blanco` int(11) NOT NULL,
  `a_rojo` int(11) NOT NULL,
  `b_blanco` int(11) NOT NULL,
  `b_rojo` int(11) NOT NULL,
  `c_blanco` int(11) NOT NULL,
  `c_rojo` int(11) NOT NULL,
  `pipo_blanco` int(11) NOT NULL,
  `pipo_rojo` int(11) NOT NULL,
  `otros_blanco` int(11) NOT NULL,
  `otros_rojo` int(11) NOT NULL,
  `huevos_vendidos` int(11) NOT NULL,
  `inven_final` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `planilla_diaria`
--

LOCK TABLES `planilla_diaria` WRITE;
/*!40000 ALTER TABLE `planilla_diaria` DISABLE KEYS */;
INSERT INTO `planilla_diaria` VALUES (1,0,0,'2017-03-25',2,0,0,0,5400,0,1500,0,0,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `planilla_diaria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prestamos`
--

DROP TABLE IF EXISTS `prestamos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prestamos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendedor` int(11) NOT NULL,
  `prestamo` int(11) NOT NULL,
  `gastos_carro` int(11) NOT NULL,
  `planilla` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prestamos`
--

LOCK TABLES `prestamos` WRITE;
/*!40000 ALTER TABLE `prestamos` DISABLE KEYS */;
/*!40000 ALTER TABLE `prestamos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yumbo_blanco` int(11) NOT NULL,
  `yumbo_rojo` int(11) NOT NULL,
  `extra_rojo` double DEFAULT NULL,
  `extra_blanco` double DEFAULT NULL,
  `aa_rojo` double DEFAULT NULL,
  `aa_blanco` double DEFAULT NULL,
  `a_blanco` double DEFAULT NULL,
  `a_rojo` double DEFAULT NULL,
  `b_blanco` double DEFAULT NULL,
  `b_rojo` double DEFAULT NULL,
  `c_blanco` double DEFAULT NULL,
  `c_rojo` double DEFAULT NULL,
  `pipo_blanco` int(11) NOT NULL,
  `pipo_rojo` int(11) NOT NULL,
  `picados_blanco` int(11) NOT NULL,
  `picados_rojo` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (1,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,'2017-03-01');
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recargue`
--

DROP TABLE IF EXISTS `recargue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recargue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `planilla` int(11) NOT NULL,
  `yumbo_blanco` int(11) NOT NULL,
  `yumbo_rojo` int(11) NOT NULL,
  `extra_blanco` int(11) NOT NULL,
  `extra_rojo` int(11) NOT NULL,
  `aa_blanco` int(11) NOT NULL,
  `aa_rojo` int(11) NOT NULL,
  `a_blanco` int(11) NOT NULL,
  `a_rojo` int(11) NOT NULL,
  `b_blanco` int(11) NOT NULL,
  `b_rojo` int(11) NOT NULL,
  `c_blanco` int(11) NOT NULL,
  `c_rojo` int(11) NOT NULL,
  `pipo_blanco` int(11) NOT NULL,
  `pipo_rojo` int(11) NOT NULL,
  `picados_blanco` int(11) NOT NULL,
  `picados_rojos` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recargue`
--

LOCK TABLES `recargue` WRITE;
/*!40000 ALTER TABLE `recargue` DISABLE KEYS */;
/*!40000 ALTER TABLE `recargue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recaudos`
--

DROP TABLE IF EXISTS `recaudos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recaudos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero_factura` int(11) NOT NULL,
  `abono` int(11) NOT NULL,
  `nuevo_saldo` int(11) NOT NULL,
  `saldo_inicial` int(11) NOT NULL,
  `planilla` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recaudos`
--

LOCK TABLES `recaudos` WRITE;
/*!40000 ALTER TABLE `recaudos` DISABLE KEYS */;
/*!40000 ALTER TABLE `recaudos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rotos`
--

DROP TABLE IF EXISTS `rotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rotos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `planilla` int(11) NOT NULL,
  `vendedor` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `yumbo_blanco` int(11) NOT NULL,
  `yumbo_rojo` int(11) NOT NULL,
  `extra_blanco` int(11) NOT NULL,
  `extra_rojo` int(11) NOT NULL,
  `aa_blanco` int(11) NOT NULL,
  `aa_rojo` int(11) NOT NULL,
  `a_blanco` int(11) NOT NULL,
  `a_rojo` int(11) NOT NULL,
  `b_blanco` int(11) NOT NULL,
  `b_rojo` int(11) NOT NULL,
  `c_blanco` int(11) NOT NULL,
  `c_rojo` int(11) NOT NULL,
  `pipo_blanco` int(11) NOT NULL,
  `pipo_rojo` int(11) NOT NULL,
  `otros_blanco` int(11) NOT NULL,
  `otros_rojo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rotos`
--

LOCK TABLES `rotos` WRITE;
/*!40000 ALTER TABLE `rotos` DISABLE KEYS */;
INSERT INTO `rotos` VALUES (13,1,2,'2017-03-25',0,0,0,2100,0,120,0,660,0,540,0,0,0,0,0,0),(15,1,2,'2017-03-25',0,0,0,1620,0,1800,0,600,0,300,0,0,0,0,0,0);
/*!40000 ALTER TABLE `rotos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','12345');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendedor`
--

DROP TABLE IF EXISTS `vendedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificacion` int(11) NOT NULL,
  `nombres` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendedor`
--

LOCK TABLES `vendedor` WRITE;
/*!40000 ALTER TABLE `vendedor` DISABLE KEYS */;
INSERT INTO `vendedor` VALUES (1,12345,'sandro estiven'),(2,123456789,'jhon pineda');
/*!40000 ALTER TABLE `vendedor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-16 16:11:33
