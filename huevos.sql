-- MySQL dump 10.16  Distrib 10.1.36-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: huevos
-- ------------------------------------------------------
-- Server version	10.1.36-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cedula_nit` int(11) NOT NULL,
  `nombres` varchar(50) NOT NULL,
  `vendedor` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (7,111111,'JOSE REYES ',1),(8,1067883800,'PUNTO DEL CHAMPIÑON ',1),(9,2222,'SANDRA ',1),(10,33333,'YANETH CHACON ',1),(11,44444,'PANADERIA ALAMOS ',1),(12,5555,'MARTHA MESA ',1),(13,6666,'RODRIGO GARMONA ',1),(14,7777,'LILIA PARDO',1),(15,88,'BENAVIDES BORJA',1),(16,99,'SALSAMENTARIA EL PAISA ',1),(17,10,'LA GRANJA ',1),(18,11,'DANIEL CASTIBLANCO',1),(19,12,'ELSA ESTEVES ',1),(20,15,'VICTOR ',1),(21,16,'SUPERMERCADO LIBANES ',1);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturas`
--

DROP TABLE IF EXISTS `facturas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `planilla` int(11) NOT NULL,
  `numero_factura` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `cliente` varchar(100) NOT NULL,
  `nit` int(11) NOT NULL,
  `tel` int(11) NOT NULL,
  `direccion` text NOT NULL,
  `bi` int(11) NOT NULL,
  `by_valor` int(11) NOT NULL,
  `baa` int(11) NOT NULL,
  `baa_valor` int(11) NOT NULL,
  `ba` int(11) NOT NULL,
  `ba_valor` int(11) NOT NULL,
  `bb` int(11) NOT NULL,
  `bb_valor` int(11) NOT NULL,
  `cb` int(11) NOT NULL,
  `cb_valor` int(11) NOT NULL,
  `jumbo` int(11) NOT NULL,
  `jumbo_valor` int(11) NOT NULL,
  `ry` int(11) NOT NULL,
  `ry_valor` int(11) NOT NULL,
  `raa` int(11) NOT NULL,
  `raa_valor` int(11) NOT NULL,
  `ra` int(11) NOT NULL,
  `ra_valor` int(11) NOT NULL,
  `rb` int(11) NOT NULL,
  `rb_valor` int(11) NOT NULL,
  `rc` int(11) NOT NULL,
  `rc_valor` int(11) NOT NULL,
  `rd` int(11) NOT NULL,
  `rd_valor` int(11) NOT NULL,
  `picados` int(11) NOT NULL,
  `picados_valor` int(11) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `total` int(11) NOT NULL,
  `vendedor` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturas`
--

LOCK TABLES `facturas` WRITE;
/*!40000 ALTER TABLE `facturas` DISABLE KEYS */;
INSERT INTO `facturas` VALUES (47,3,6200,'2020-11-11','JOSE REYES ',111111,11111111,'11111',0,0,0,0,0,0,0,0,0,0,0,0,0,0,90,310,0,0,0,0,0,0,0,0,0,0,'cancelado',27900,1),(48,3,6199,'2020-11-11','PUNTO DEL CHAMPIÑON ',1067883800,9311548,'CARRERA 49*131-25',0,0,0,0,0,0,0,0,0,0,0,0,750,315,600,295,0,0,0,0,0,0,0,0,0,0,'credito',413250,1),(49,3,6197,'2020-11-11','SANDRA ',2222,22222,'22222222',0,0,0,0,0,0,0,0,0,0,0,0,90,330,0,0,120,286,0,0,0,0,0,0,0,0,'cancelado',64020,1),(50,3,6196,'2020-11-11','YANETH CHACON ',33333,33333,'333333',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,450,280,0,0,0,0,0,0,0,0,'cancelado',126000,1),(51,3,6194,'2020-11-11','PANADERIA ALAMOS ',44444,444444,'444444',0,0,0,0,0,0,0,0,0,0,0,0,0,0,300,310,0,0,0,0,0,0,0,0,0,0,'cancelado',93000,1),(52,3,6193,'2020-11-11','MARTHA MESA ',5555,55555,'5555',0,0,0,0,0,0,0,0,0,0,0,0,0,0,150,310,0,0,0,0,0,0,0,0,0,0,'cancelado',46500,1),(53,3,6192,'2020-11-11','RODRIGO GARMONA ',6666,66666,'666666',0,0,0,0,0,0,0,0,0,0,0,0,0,0,300,300,300,290,0,0,0,0,0,0,0,0,'cancelado',177000,1),(54,3,6191,'2020-11-11','LILIA PARDO',7777,77,'7777',0,0,0,0,0,0,0,0,0,0,0,0,0,0,60,300,0,0,0,0,0,0,0,0,0,0,'cancelado',18000,1),(55,3,6190,'2020-11-11','BENAVIDES BORJA',88,8888,'888',0,0,0,0,0,0,0,0,0,0,0,0,0,0,300,310,0,0,0,0,0,0,0,0,0,0,'credito',93000,1),(56,3,6189,'2020-11-11','SALSAMENTARIA EL PAISA ',99,9,'9',0,0,0,0,0,0,0,0,0,0,0,0,0,0,600,310,0,0,0,0,0,0,0,0,0,0,'cancelado',186000,1),(57,3,6188,'2020-11-11','LA GRANJA ',10,100,'1000',0,0,0,0,0,0,0,0,0,0,0,0,0,0,120,310,120,290,0,0,0,0,0,0,0,0,'cancelado',72000,1),(58,3,6187,'2020-11-11','DANIEL CASTIBLANCO',11,11,'1111',0,0,300,290,0,0,0,0,0,0,0,0,0,0,150,310,0,0,0,0,0,0,0,0,0,0,'credito',133500,1),(59,3,6186,'2020-11-11','ELSA ESTEVES ',12,12,'SUBA ',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,210,290,0,0,0,0,0,0,0,0,'cancelado',60900,1),(60,3,6198,'2020-11-11','VICTOR ',15,511,'15',0,0,0,0,0,0,0,0,0,0,0,0,0,0,240,305,0,0,0,0,0,0,0,0,0,0,'cancelado',73200,1),(61,3,6195,'2020-11-11','SUPERMERCADO LIBANES ',16,16,'16',0,0,0,0,0,0,0,0,0,0,0,0,0,0,600,310,0,0,0,0,0,0,0,0,0,0,'cancelado',186000,1);
/*!40000 ALTER TABLE `facturas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ini`
--

DROP TABLE IF EXISTS `ini`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ini` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yumbo_blanco` int(11) NOT NULL,
  `yumbo_rojo` int(11) NOT NULL,
  `extra_blanco` int(11) NOT NULL,
  `extra_rojo` int(11) NOT NULL,
  `aa_blanco` int(11) NOT NULL,
  `aa_rojo` int(11) NOT NULL,
  `a_blanco` int(11) NOT NULL,
  `a_rojo` int(11) NOT NULL,
  `b_blanco` int(11) NOT NULL,
  `b_rojo` int(11) NOT NULL,
  `c_blanco` int(11) NOT NULL,
  `c_rojo` int(11) NOT NULL,
  `pipo_blanco` int(11) NOT NULL,
  `pipo_rojo` int(11) NOT NULL,
  `otros_blanco` int(11) NOT NULL,
  `otros_rojo` int(11) NOT NULL,
  `vendedor` int(11) NOT NULL,
  `planilla` int(11) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ini`
--

LOCK TABLES `ini` WRITE;
/*!40000 ALTER TABLE `ini` DISABLE KEYS */;
INSERT INTO `ini` VALUES (24,0,0,0,2160,600,9290,0,417,0,540,0,0,0,0,0,0,1,3,'2020-11-11');
/*!40000 ALTER TABLE `ini` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `planilla_diaria`
--

DROP TABLE IF EXISTS `planilla_diaria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planilla_diaria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yumbo_blanco` int(11) NOT NULL,
  `yumbo_rojo` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `vendedor` int(100) NOT NULL,
  `extra_blanco` int(11) NOT NULL,
  `extra_rojo` int(11) NOT NULL,
  `aa_blanco` int(11) NOT NULL,
  `aa_rojo` int(11) NOT NULL,
  `a_blanco` int(11) NOT NULL,
  `a_rojo` int(11) NOT NULL,
  `b_blanco` int(11) NOT NULL,
  `b_rojo` int(11) NOT NULL,
  `c_blanco` int(11) NOT NULL,
  `c_rojo` int(11) NOT NULL,
  `pipo_blanco` int(11) NOT NULL,
  `pipo_rojo` int(11) NOT NULL,
  `otros_blanco` int(11) NOT NULL,
  `otros_rojo` int(11) NOT NULL,
  `huevos_vendidos` int(11) NOT NULL,
  `inven_final` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `planilla_diaria`
--

LOCK TABLES `planilla_diaria` WRITE;
/*!40000 ALTER TABLE `planilla_diaria` DISABLE KEYS */;
INSERT INTO `planilla_diaria` VALUES (3,0,0,'2020-11-11',1,0,3000,900,11960,0,1617,0,540,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `planilla_diaria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prestamos`
--

DROP TABLE IF EXISTS `prestamos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prestamos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendedor` int(11) NOT NULL,
  `prestamo` int(11) NOT NULL,
  `gastos_carro` int(11) NOT NULL,
  `planilla` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prestamos`
--

LOCK TABLES `prestamos` WRITE;
/*!40000 ALTER TABLE `prestamos` DISABLE KEYS */;
/*!40000 ALTER TABLE `prestamos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yumbo_blanco` int(11) NOT NULL,
  `yumbo_rojo` int(11) NOT NULL,
  `extra_rojo` double DEFAULT NULL,
  `extra_blanco` double DEFAULT NULL,
  `aa_rojo` double DEFAULT NULL,
  `aa_blanco` double DEFAULT NULL,
  `a_blanco` double DEFAULT NULL,
  `a_rojo` double DEFAULT NULL,
  `b_blanco` double DEFAULT NULL,
  `b_rojo` double DEFAULT NULL,
  `c_blanco` double DEFAULT NULL,
  `c_rojo` double DEFAULT NULL,
  `pipo_blanco` int(11) NOT NULL,
  `pipo_rojo` int(11) NOT NULL,
  `picados_blanco` int(11) NOT NULL,
  `picados_rojo` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (1,0,0,305,0,285,265,0,265,0,240,0,0,0,0,0,0,'2020-11-11');
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recargue`
--

DROP TABLE IF EXISTS `recargue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recargue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `planilla` int(11) NOT NULL,
  `yumbo_blanco` int(11) NOT NULL,
  `yumbo_rojo` int(11) NOT NULL,
  `extra_blanco` int(11) NOT NULL,
  `extra_rojo` int(11) NOT NULL,
  `aa_blanco` int(11) NOT NULL,
  `aa_rojo` int(11) NOT NULL,
  `a_blanco` int(11) NOT NULL,
  `a_rojo` int(11) NOT NULL,
  `b_blanco` int(11) NOT NULL,
  `b_rojo` int(11) NOT NULL,
  `c_blanco` int(11) NOT NULL,
  `c_rojo` int(11) NOT NULL,
  `pipo_blanco` int(11) NOT NULL,
  `pipo_rojo` int(11) NOT NULL,
  `picados_blanco` int(11) NOT NULL,
  `picados_rojos` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recargue`
--

LOCK TABLES `recargue` WRITE;
/*!40000 ALTER TABLE `recargue` DISABLE KEYS */;
/*!40000 ALTER TABLE `recargue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recaudos`
--

DROP TABLE IF EXISTS `recaudos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recaudos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero_factura` int(11) NOT NULL,
  `abono` int(11) NOT NULL,
  `nuevo_saldo` int(11) NOT NULL,
  `saldo_inicial` int(11) NOT NULL,
  `planilla` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recaudos`
--

LOCK TABLES `recaudos` WRITE;
/*!40000 ALTER TABLE `recaudos` DISABLE KEYS */;
/*!40000 ALTER TABLE `recaudos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rotos`
--

DROP TABLE IF EXISTS `rotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rotos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `planilla` int(11) NOT NULL,
  `vendedor` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `yumbo_blanco` int(11) NOT NULL,
  `yumbo_rojo` int(11) NOT NULL,
  `extra_blanco` int(11) NOT NULL,
  `extra_rojo` int(11) NOT NULL,
  `aa_blanco` int(11) NOT NULL,
  `aa_rojo` int(11) NOT NULL,
  `a_blanco` int(11) NOT NULL,
  `a_rojo` int(11) NOT NULL,
  `b_blanco` int(11) NOT NULL,
  `b_rojo` int(11) NOT NULL,
  `c_blanco` int(11) NOT NULL,
  `c_rojo` int(11) NOT NULL,
  `pipo_blanco` int(11) NOT NULL,
  `pipo_rojo` int(11) NOT NULL,
  `otros_blanco` int(11) NOT NULL,
  `otros_rojo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rotos`
--

LOCK TABLES `rotos` WRITE;
/*!40000 ALTER TABLE `rotos` DISABLE KEYS */;
INSERT INTO `rotos` VALUES (16,3,1,'2020-11-11',0,0,0,2160,600,9238,0,410,0,540,0,0,0,0,0,0);
/*!40000 ALTER TABLE `rotos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','12345');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendedor`
--

DROP TABLE IF EXISTS `vendedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificacion` int(11) NOT NULL,
  `nombres` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendedor`
--

LOCK TABLES `vendedor` WRITE;
/*!40000 ALTER TABLE `vendedor` DISABLE KEYS */;
INSERT INTO `vendedor` VALUES (1,12345,'Leidy Racines'),(2,123456789,'Jhon Pineda');
/*!40000 ALTER TABLE `vendedor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-15 19:34:31
